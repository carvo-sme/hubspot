package ch.carvo.hubspotcrawler;

import ch.carvo.hubspotcrawler.dto.Property;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Helper {

    private static Helper instance;
    private Map<String, String> dealStage;
    private Map<String, String> leadStage;

    private Helper() {
        dealStage = new HashMap<>();
        dealStage.put("217296", "Bonität OK");
        dealStage.put("97a6302d-b3a4-4d63-ad56-81485f95d010", "Fahrzeug ausgewählt");
        dealStage.put("1b1311b2-a384-4a80-8ab2-3ce78ebbce1f", "Rechnung versandt");
        dealStage.put("3e0d84fc-f0be-4571-b058-9fc9f59e7ade", "Anfangspauschale bezahlt");
        dealStage.put("94be1217-fcce-4dd8-88a0-301eb45ac316", "Won - Erste Rechnung gezahlt");
        dealStage.put("eaad79cd-04ec-497d-92db-f108945b8584", "Absage");
        dealStage.put("a94b8ba4-7611-4f5d-a112-f2c077a2d0f2", "Churn");
        dealStage.put("1c2b992f-ff87-4157-9c83-6d4dcf861c96", "AP bezahlt|Kunde wartet auf uns");

        leadStage = new HashMap<>();
        leadStage.put("NEW", "Neu");
        leadStage.put("attempted_to_contact", "Kontaktversuch");
        leadStage.put("Connected", "Interaktion");
        leadStage.put("Open deal", "Offener Deal");
        leadStage.put("Unqualified", "Unqualifiziert");
        leadStage.put("CHURN", "Churn");
        leadStage.put("Kunde", "Kunde");
    }

    public static Helper get() {
        if (instance == null) {
            instance = new Helper();
        }
        return instance;
    }

    public String getReadableDealstage(String hash) {
        if (dealStage.get(hash) == null) {
            return hash;
        }
        return dealStage.get(hash);
    }

    public String getReadableLeadStage(String key) {
        if (leadStage.get(key) == null) {
            return key;
        }
        return leadStage.get(key);
    }

    public Set<Property> contactProps() {
        Set<Property> props = new LinkedHashSet<>();
        props.add(Property.CONTACT_CONTACT_LOST);
        props.add(Property.CONTACT_CONTACT_LOST_DATE);
        props.add(Property.CONTACT_LEAD_STATUS);
        props.add(Property.CONTACT_LIFECYCLE_STAGE);
        props.add(Property.CONTACT_MQL_DATE);
        props.add(Property.CONTACT_SQL_DATE);
        props.add(Property.CONTACT_OPP_DATE);
        props.add(Property.CONTACT_LEAD_DATE);
        props.add(Property.DEFAULT_LAST_MODIFICATION);
        props.add(Property.CONTACT_FIRST_NAME);
        props.add(Property.CONTACT_LAST_NAME);
        props.add(Property.CONTACT_CUSTOMER_DATE);
        props.add(Property.CONTACT_SOURCE);
        props.add(Property.HS_ANALYTICS_SOURCE);
        return props;
    }

    public Set<Property> dealProps() {
        Set<Property> props = new LinkedHashSet<>();
        props.add(Property.DEAL_NAME);
        props.add(Property.CLOSE_DATE);
        props.add(Property.CREATE_DATE);
        props.add(Property.DEAL_STAGE);
        props.add(Property.HS_ANALYTICS_SOURCE);
        props.add(Property.HS_OBJECT_ID);
        props.add(Property.FIRST_NAME);
        props.add(Property.LAST_NAME);
        props.add(Property.CONTRACT_CLOSED);
        props.add(Property.CONTRACT_SIGNED);
        return props;
    }

    public static Double deltaDays(Timestamp start, Timestamp end) {
        if (start == null && end == null || start != null && end == null) {
            return null;
        }

        if (start.after(end)) {
            return 0d;
        }

        Long hours = (end.getTime() - start.getTime()) / (1000 * 60 * 60);
        Double d = (double) hours / 24;
        return (double) Math.round(d * 100) / 100;
    }
}
