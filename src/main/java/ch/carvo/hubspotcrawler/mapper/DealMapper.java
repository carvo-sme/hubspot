package ch.carvo.hubspotcrawler.mapper;

import ch.carvo.hubspotcrawler.Helper;
import ch.carvo.hubspotcrawler.db.Contact;
import ch.carvo.hubspotcrawler.db.Database;
import ch.carvo.hubspotcrawler.db.Deal;
import ch.carvo.hubspotcrawler.dto.DealDTO;
import ch.carvo.hubspotcrawler.dto.Property;

import java.util.List;
import java.util.stream.Collectors;

public class DealMapper {

    public Deal map(DealDTO dto) {
        Deal deal = new Deal();
        deal.setDealId(dto.getDealId());
        deal.setPortalId(dto.getPortalId());
        deal.setIsDeleted(dto.getDeleted());
        deal.setClosedate(dto.getPropertyNameAsLong(Property.CLOSE_DATE));
        deal.setCreatedate(dto.getPropertyNameAsLong(Property.CREATE_DATE));
        deal.setDealstage(Helper.get().getReadableDealstage(dto.getProperty(Property.DEAL_STAGE)));
        deal.setDealname(dto.getProperty(Property.DEAL_NAME));
        deal.setFirstname(dto.getProperty(Property.FIRST_NAME));
        deal.setLastname(dto.getProperty(Property.LAST_NAME));
        deal.setContract_closed(dto.getProperty(Property.CONTRACT_CLOSED));
        deal.setContract_signed(dto.getProperty(Property.CONTRACT_SIGNED));
        deal.setVid(dto.getAssociatedVid());

        Contact c = Database.getInstance().getByVid(deal.getVid());

        if (c != null && c.getLeadDate() != null) {
            deal.setFulltime(Helper.deltaDays(c.getLeadDate(), deal.getClosedate()));
            deal.setDealtime(Helper.deltaDays(deal.getCreatedate(), deal.getClosedate()));
        }

        return deal;
    }

    public List<Deal> map(List<DealDTO> dtos) {
        return dtos.stream().map(this::map).collect(Collectors.toList());
    }

    public Long getLong(Long a, Long b) {
        if (a - b < 0) {
            return null;
        } else {
            return a - b;
        }
    }
}
