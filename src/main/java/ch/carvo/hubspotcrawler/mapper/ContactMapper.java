package ch.carvo.hubspotcrawler.mapper;

import ch.carvo.hubspotcrawler.Helper;
import ch.carvo.hubspotcrawler.db.Contact;
import ch.carvo.hubspotcrawler.dto.ContactDTO;
import ch.carvo.hubspotcrawler.dto.Property;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ContactMapper {

    public List<Contact> map(List<ContactDTO> contacts) {
        List<Contact> result = new ArrayList<>(contacts.size());
        for (ContactDTO dto : contacts) {
            result.add(map(dto));
        }
        return result;
    }

    public Contact map(ContactDTO dto) {
        Contact contact = new Contact();
        contact.setVid(dto.getVid());
        contact.setPortalId(dto.getPortalId());
        contact.setAddedAt(new Timestamp(dto.getAddedAt().getTime()));

        contact.setContactLost(dto.getProperty(Property.CONTACT_CONTACT_LOST));
        contact.setLifecycleStage(dto.getProperty(Property.CONTACT_LIFECYCLE_STAGE));
        contact.setOppdate(dto.getPropertyAsTimestamp(Property.CONTACT_OPP_DATE));
        contact.setSqldate(dto.getPropertyAsTimestamp(Property.CONTACT_SQL_DATE));
        contact.setMqldate(dto.getPropertyAsTimestamp(Property.CONTACT_MQL_DATE));
        contact.setContactLostDate(dto.getPropertyAsTimestamp(Property.CONTACT_CONTACT_LOST_DATE));
        contact.setLeadDate(dto.getPropertyAsTimestamp(Property.CONTACT_LEAD_DATE));
        contact.setFirstName(dto.getProperty(Property.CONTACT_FIRST_NAME));
        contact.setLastName(dto.getProperty(Property.CONTACT_LAST_NAME));
        contact.setCompany(dto.getProperty(Property.CONTACT_COMPANY));
        contact.setLeadStatus(Helper.get().getReadableLeadStage(dto.getProperty(Property.CONTACT_LEAD_STATUS)));
        contact.setCustomerdate(dto.getPropertyAsTimestamp(Property.CONTACT_CUSTOMER_DATE));
        contact.setSource(dto.getProperty(Property.CONTACT_SOURCE));
        contact.setHsAnalyticsSource(dto.getProperty(Property.HS_ANALYTICS_SOURCE));
//        contact.setLastModified(new Timestamp(Long.parseLong(dto.getLastmodifieddate())));

        return cleanup(contact);
    }

    private Contact cleanup(Contact c) {
        c = checkDates(c);
        return c;
    }

    public Contact checkDates(Contact c) {
        if (c.getCustomerdate() != null) {
            if (c.getOppdate() == null) {
                c.setOppdate(c.getCustomerdate());
            }
            if (c.getSqldate() == null) {
                c.setSqldate(c.getCustomerdate());
            }
            if (c.getMqldate() == null) {
                c.setMqldate(c.getCustomerdate());
            }
        } else if (c.getOppdate() != null) {
            if (c.getSqldate() == null) {
                c.setSqldate(c.getOppdate());
            }
            if (c.getMqldate() == null) {
                c.setMqldate(c.getOppdate());
            }
        } else if (c.getSqldate() != null) {
            if (c.getMqldate() == null) {
                c.setMqldate(c.getSqldate());
            }
        }

        if (c.getLeadDate() == null && c.getMqldate() != null) {
            c.setLeadDate(c.getMqldate());
        }

//        if (!c.getContactLost().isEmpty() && c.getContactLostDate() == null) {
//            // time set to 27.02.2019
//            c.setContactLostDate(new Timestamp(1551268800000L));
//        }

        if (c.getCustomerdate() != null && c.getLeadDate() != null) {
            c.setCustomerDeltaTime(Helper.deltaDays(c.getLeadDate(), c.getCustomerdate()));
        }
        return c;
    }
}
