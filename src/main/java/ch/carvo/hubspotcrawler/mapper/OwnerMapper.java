package ch.carvo.hubspotcrawler.mapper;

import ch.carvo.hubspotcrawler.db.Owner;
import ch.carvo.hubspotcrawler.dto.OwnerDTO;
import java.util.List;
import java.util.stream.Collectors;

public class OwnerMapper {

    public Owner map(OwnerDTO dto) {
        Owner owner = new Owner();
        owner.setActiveUserId(dto.getActiveUserId());
        owner.setEmail(dto.getEmail());
        owner.setFirstname(dto.getFirstName());
        owner.setHasContactsAccess(dto.getHasContactsAccess());
        owner.setIsActive(dto.getIsActive());
        owner.setLastname(dto.getLastName());
        owner.setOwnerId(dto.getOwnerId());
        owner.setPortalId(dto.getPortalId());
        owner.setType(dto.getType());
        return owner;
    }

    public List<Owner> map(List<OwnerDTO> dtos) {
        return dtos.stream().map(this::map).collect(Collectors.toList());
    }
}
