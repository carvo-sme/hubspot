package ch.carvo.hubspotcrawler.db;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

public class Database {

    private static Database instance;
    private boolean alreadyInitialized = false;
    private JdbcConnectionSource con;
    private Dao<Deal, Long> dealTable;
    private Dao<Contact, Long> contactTable;
    private Dao<Owner, Long> ownerTable;

    private final Logger LOGGER = LoggerFactory.getLogger(Database.class);

    private Database() {
        init();
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    private void init() {
        if (!alreadyInitialized) {
            initDB();
        }
    }

    private void initDB() {
        try {
            con = new JdbcPooledConnectionSource("jdbc:mysql://localhost/hubspot?user=hubspot&password=hubspot");
            dealTable = DaoManager.createDao(con, Deal.class);
            contactTable = DaoManager.createDao(con, Contact.class);
            ownerTable = DaoManager.createDao(con, Owner.class);

            TableUtils.createTableIfNotExists(con, Deal.class);
            TableUtils.createTableIfNotExists(con, Contact.class);
            TableUtils.createTableIfNotExists(con, Owner.class);

            TableUtils.clearTable(con, Deal.class);
            TableUtils.clearTable(con, Contact.class);
            TableUtils.clearTable(con, Owner.class);
        } catch (SQLException ex) {
            LOGGER.error("Caught Exception during db initialization", ex);
        }
    }

    public void createOrUpdateDeals(List<Deal> deals) {
        if (deals == null || deals.isEmpty()) {
            return;
        }
        for (Deal d : deals) {
            createOrUpdateDeal(d);
        }
    }

    public void createOrUpdateDeal(Deal deal) {
        try {
            dealTable.createOrUpdate(deal);
        } catch (SQLException ex) {
            LOGGER.error("Caught exception during deal update", ex);
        }
    }

    public void createOrUpdateContacts(List<Contact> contacts) {
        if (contacts == null || contacts.isEmpty()) {
            return;
        }
        for (Contact contact : contacts) {
            createOrUpdateContact(contact);
        }
    }

    private void createOrUpdateContact(Contact contact) {
        try {
            contactTable.createOrUpdate(contact);
        } catch (SQLException ex) {
            LOGGER.error("Caught exception during contact update:", ex);
        }
    }

    public void createOrUpdateOwners(List<Owner> owners) {
        if (owners == null || owners.isEmpty()) {
            return;
        }
        for (Owner owner : owners) {
            createOrUpdateOwner(owner);
        }
    }

    private void createOrUpdateOwner(Owner owner) {
        try {
            ownerTable.createOrUpdate(owner);
        } catch (SQLException ex) {
            LOGGER.error("Caught exception during owner update:", ex);
        }
    }

    public Contact getByVid(Long id) {
        try {
            return contactTable.queryBuilder().where().eq("vid", id).queryForFirst();
        } catch (SQLException ex) {
            return null;
        }
    }
}
