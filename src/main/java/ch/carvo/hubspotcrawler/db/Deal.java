package ch.carvo.hubspotcrawler.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.sql.Timestamp;

@DatabaseTable(tableName = "deal")
public class Deal extends BaseTable {

    @DatabaseField
    private Long vid;
    @DatabaseField
    private Long dealId;
    @DatabaseField
    private Long portalId;
    @DatabaseField
    private Boolean isDeleted;
    @DatabaseField
    private String dealname;
    @DatabaseField
    private String contract_closed;
    @DatabaseField
    private String contract_signed;
    @DatabaseField
    private String firstname;
    @DatabaseField
    private String lastname;
    @DatabaseField
    private String dealstage;
    @DatabaseField
    private Timestamp createdate;
    @DatabaseField
    private Timestamp closedate;
    @DatabaseField
    private Double dealtime;
    @DatabaseField
    private Double fulltime;

    public Deal() {
    }

    public Long getDealId() {
        return dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public String getDealname() {
        return dealname;
    }

    public void setDealname(String dealname) {
        this.dealname = dealname;
    }

    public String getDealstage() {
        return dealstage;
    }

    public void setDealstage(String dealstage) {
        this.dealstage = dealstage;
    }

    public Timestamp getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Timestamp createdate) {
        this.createdate = createdate;
    }

    public void setCreatedate(Long createdate) {
        if (createdate == null) {
            this.createdate = null;
        } else {
            this.createdate = new Timestamp(createdate);
        }
    }

    public Timestamp getClosedate() {
        return closedate;
    }

    public void setClosedate(Timestamp closedate) {
        this.closedate = closedate;
    }

    public void setClosedate(Long closedate) {
        if (closedate == null) {
            this.closedate = null;
        } else {
            this.closedate = new Timestamp(closedate);
        }
    }

    @Override
    public String toString() {
        return "Deal{" + "dealId=" + dealId + ", portalId=" + portalId + ", isDeleted=" + isDeleted + ", dealname=" + dealname + ", dealstage=" + dealstage + ", createdate=" + createdate + ", closedate=" + closedate + ", dealtime=" + dealtime + '}';
    }

    public Long getVid() {
        return vid;
    }

    public void setVid(Long vid) {
        this.vid = vid;
    }

    public String getContract_closed() {
        return contract_closed;
    }

    public void setContract_closed(String contract_closed) {
        this.contract_closed = contract_closed;
    }

    public String getContract_signed() {
        return contract_signed;
    }

    public void setContract_signed(String contract_signed) {
        this.contract_signed = contract_signed;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Double getDealtime() {
        return dealtime;
    }

    public void setDealtime(Double dealtime) {
        this.dealtime = dealtime;
    }

    public Double getFulltime() {
        return fulltime;
    }

    public void setFulltime(Double fulltime) {
        this.fulltime = fulltime;
    }


    
}
