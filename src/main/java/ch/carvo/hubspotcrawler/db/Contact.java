package ch.carvo.hubspotcrawler.db;

import com.j256.ormlite.field.DatabaseField;

import java.sql.Timestamp;

public class Contact extends BaseTable {

    @DatabaseField
    private Long contactId;
    @DatabaseField
    private Long vid;
    @DatabaseField
    private Long portalId;
    @DatabaseField
    private Timestamp addedAt;
    @DatabaseField
    private String firstName;
    @DatabaseField
    private String lastName;
    @DatabaseField
    private String company;
    @DatabaseField
    private Timestamp lastModified;
    @DatabaseField
    private Double customerDeltaTime;
    @DatabaseField
    private Timestamp leadDate;
    @DatabaseField
    private Timestamp mqldate;
    @DatabaseField
    private Timestamp sqldate;
    @DatabaseField
    private Timestamp oppdate;
    @DatabaseField
    private Timestamp customerdate;
    @DatabaseField
    private Timestamp contactLostDate;
    @DatabaseField
    private String leadStatus;
    @DatabaseField
    private String contactLost;
    @DatabaseField
    private String lifecycleStage;
    @DatabaseField
    private String source;
    @DatabaseField
    private String hsAnalyticsSource;

    public Contact() {
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Timestamp getAddedAt() {
        return addedAt;
    }

    public void setAddedAt(Timestamp addedAt) {
        this.addedAt = addedAt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Timestamp getLastModified() {
        return lastModified;
    }

    public void setLastModified(Timestamp lastModified) {
        this.lastModified = lastModified;
    }

    public Timestamp getMqldate() {
        return mqldate;
    }

    public void setMqldate(Timestamp mqldate) {
        this.mqldate = mqldate;
    }

    public Timestamp getSqldate() {
        return sqldate;
    }

    public void setSqldate(Timestamp sqldate) {
        this.sqldate = sqldate;
    }

    public Timestamp getOppdate() {
        return oppdate;
    }

    public void setOppdate(Timestamp oppdate) {
        this.oppdate = oppdate;
    }

    public Timestamp getContactLostDate() {
        return contactLostDate;
    }

    public void setContactLostDate(Timestamp contactLostDate) {
        this.contactLostDate = contactLostDate;
    }

    public String getLeadStatus() {
        return leadStatus;
    }

    public void setLeadStatus(String leadStatus) {
        this.leadStatus = leadStatus;
    }

    public String getContactLost() {
        return contactLost;
    }

    public void setContactLost(String contactLost) {
        this.contactLost = contactLost;
    }

    public String getLifecycleStage() {
        return lifecycleStage;
    }

    public void setLifecycleStage(String lifecycleStage) {
        this.lifecycleStage = lifecycleStage;
    }

    public Timestamp getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(Timestamp leadDate) {
        this.leadDate = leadDate;
    }

    public Timestamp getCustomerdate() {
        return customerdate;
    }

    public void setCustomerdate(Timestamp customerdate) {
        this.customerdate = customerdate;
    }

    public Long getVid() {
        return vid;
    }

    public void setVid(Long vid) {
        this.vid = vid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getHsAnalyticsSource() {
        return hsAnalyticsSource;
    }

    public void setHsAnalyticsSource(String hsAnalyticsSource) {
        this.hsAnalyticsSource = hsAnalyticsSource;
    }

    public Double getCustomerDeltaTime() {
        return customerDeltaTime;
    }

    public void setCustomerDeltaTime(Double customerDeltaTime) {
        this.customerDeltaTime = customerDeltaTime;
    }
}
