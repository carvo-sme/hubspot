package ch.carvo.hubspotcrawler.db;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class DB {

    private static DB instance;
    private boolean alreadyInit = false;

    private DB() {
        init();
    }

    public static DB instance() {
        if (instance == null) {
            instance = new DB();
        }
        return instance;
    }

    private void init() {
        if (!alreadyInit) {
            initDB();
        }
    }

    private void initDB() {
        Connection conn = null;
        Statement stmt = null;
    }
}
