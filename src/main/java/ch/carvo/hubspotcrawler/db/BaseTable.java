package ch.carvo.hubspotcrawler.db;

import com.j256.ormlite.field.DatabaseField;

public abstract class BaseTable {

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    protected Long id;

    public BaseTable() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
