package ch.carvo.hubspotcrawler.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "owner")
public class Owner extends BaseTable {

    @DatabaseField
    @JsonProperty("portalId")
    private Long portalId;
    @DatabaseField
    private Long ownerId;
    @DatabaseField
    private String type;
    @DatabaseField
    @JsonProperty("firstName")
    private String firstname;
    @DatabaseField
    @JsonProperty("lastName")
    private String lastname;
    @DatabaseField
    private String email;
    @DatabaseField
    private Boolean hasContactsAccess;
    @DatabaseField
    private Long activeUserId;
    @DatabaseField
    private Boolean isActive;

    public Owner() {
    }

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getHasContactsAccess() {
        return hasContactsAccess;
    }

    public void setHasContactsAccess(Boolean hasContactsAccess) {
        this.hasContactsAccess = hasContactsAccess;
    }

    public Long getActiveUserId() {
        return activeUserId;
    }

    public void setActiveUserId(Long activeUserId) {
        this.activeUserId = activeUserId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
