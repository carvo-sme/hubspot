package ch.carvo.hubspotcrawler;

import ch.carvo.hubspotcrawler.db.Database;
import ch.carvo.hubspotcrawler.db.Deal;
import ch.carvo.hubspotcrawler.dto.*;
import ch.carvo.hubspotcrawler.mapper.ContactMapper;
import ch.carvo.hubspotcrawler.mapper.DealMapper;
import ch.carvo.hubspotcrawler.mapper.OwnerMapper;

import java.util.List;

public class Main {

    private static final int LIMIT_CONTACTS = 100;
    private static final int LIMIT_DEALS = 250;
    private static final Hubspot hubspot = new Hubspot();
    private static DealMapper dealMapper = new DealMapper();
    private static ContactMapper contactMapper = new ContactMapper();
    private static OwnerMapper ownersMapper = new OwnerMapper();
    private static Database db = Database.getInstance();

    public static void main(String[] args) throws Exception {
        contacts();
        deals();
        owners();
    }

    private static void deals() throws InterruptedException {
        int processed = 0;
        DealResponse dealResponse;
        do {
            dealResponse = hubspot.queryDeals(LIMIT_DEALS, processed);
            List<DealDTO> deals = dealResponse.getDeals();
            List<Deal> dbDeals = dealMapper.map(deals);
            db.createOrUpdateDeals(dbDeals);
            processed += deals.size();
            Thread.sleep(100);
        } while (dealResponse.getHasMore());
    }

    private static void contacts() throws InterruptedException {
        int processed = 0;
        ContactResponse contactResponse;
        do {
            contactResponse = hubspot.queryContacts(LIMIT_CONTACTS, processed);
            List<ContactDTO> contacts = contactResponse.getContacts();
            db.createOrUpdateContacts(contactMapper.map(contacts));
            processed += contacts.size();
            Thread.sleep(100);
            if (contactResponse.getHasMore()) {
                processed = contactResponse.getVidOffset().intValue();
            }
        } while (contactResponse.getHasMore());
    }

    private static void owners() {
        List<OwnerDTO> owners = hubspot.queryOwners();
        db.createOrUpdateOwners(ownersMapper.map(owners));
    }
}
