package ch.carvo.hubspotcrawler;

import ch.carvo.hubspotcrawler.dto.ContactResponse;
import ch.carvo.hubspotcrawler.dto.DealResponse;
import ch.carvo.hubspotcrawler.dto.HapiPath;
import ch.carvo.hubspotcrawler.dto.OwnerDTO;
import ch.carvo.hubspotcrawler.dto.OwnerResponse;
import ch.carvo.hubspotcrawler.dto.Property;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class Hubspot {

    private final static String BASE_URI = "https://api.hubapi.com";
    private final static String API_KEY = "c15fdb38-bb4a-4eb5-adc9-ade1c799f534";

    private final Set<Property> properties = new LinkedHashSet<>();

    private Boolean includeAssociations = true;

    private ObjectMapper objectMapper = new ObjectMapper();

    private static final Logger LOGGER = LoggerFactory.getLogger(Hubspot.class);

    private Map<Class<?>, String> pathCache = new HashMap<>();

    public Hubspot() {
    }

    public DealResponse queryDeals(int limit, int offset) {
        setQueryProperties(Helper.get().dealProps());
        QueryParamBuilder params = buildDealPropertyQuery("properties");
        try {
            return queryAPI(DealResponse.class, params, limit, offset, "limit", "offset");
        } catch (Exception e) {
            throw new RuntimeException("Exception caught during deals request", e);
        }
    }

    public ContactResponse queryContacts(int limit, int offset) {
        setQueryProperties(Helper.get().contactProps());
        QueryParamBuilder params = buildDealPropertyQuery("property");
        try {
            return queryAPI(ContactResponse.class, params, limit, offset, "count", "vidOffset");
        } catch (Exception e) {
            throw new RuntimeException("Exception caught during contacts request", e);
        }
    }

    public List<OwnerDTO> queryOwners() {
        HttpGet get = new HttpGet();

        get.setURI(buildURI(determinePath(OwnerResponse.class), null, 100, 0, "", ""));
        List<OwnerDTO> response = new ArrayList<>();
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            CloseableHttpResponse execute = client.execute(get);
            InputStream content = execute.getEntity().getContent();
            response = objectMapper.readValue(content, new TypeReference<List<OwnerDTO>>() {
            });
        } catch (Exception e) {
            throw new RuntimeException("Exception caught during deals request", e);
        }
        return response;
    }

    private <T> T queryAPI(Class<T> resultClass, QueryParamBuilder params, int limit, int offset, String limitName, String offsetName) throws IOException {
        HttpGet get = new HttpGet();

        get.setURI(buildURI(determinePath(resultClass), params, limit, offset, limitName, offsetName));
        T response;
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            CloseableHttpResponse execute = client.execute(get);
            InputStream content = execute.getEntity().getContent();
            response = objectMapper.readValue(content, resultClass);
        }
        return response;
    }

    private <T> String determinePath(Class<T> dtoClass) {
        String result = pathCache.get(dtoClass);
        if (result != null) {
            return result;
        }
        HapiPath hapiPath = dtoClass.getAnnotation(HapiPath.class);
        if (hapiPath == null) {
            throw new RuntimeException("HapiPath annotation not found on " + dtoClass);
        }
        pathCache.put(dtoClass, hapiPath.value());
        return hapiPath.value();
    }

    private URI buildURI(String path, QueryParamBuilder paramBuilder, int limit, int offset, String limitName, String offsetName) {
        QueryParamBuilder builder = new QueryParamBuilder();
        builder.add("hapikey", API_KEY);
        builder.add(paramBuilder)
                .add(limitName, String.valueOf(limit))
                .add(offsetName, String.valueOf(offset));
        try {
            String url = BASE_URI + path + builder.build();
            LOGGER.debug("Built url: " + url);
            return new URI(url);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private QueryParamBuilder buildDealPropertyQuery(String propertyname) {
        QueryParamBuilder result = new QueryParamBuilder();
        result.add("includeAssociations", includeAssociations.toString());
        for (Property p : properties) {
            result.add(propertyname, p.toString());
        }
        return result;
    }

    public void setQueryProperties(Collection<Property> properties) {
        this.properties.clear();
        this.properties.addAll(properties);
    }

    public void enable(Property property) {
        this.properties.add(property);
    }

    public void disable(Property property) {
        this.properties.remove(property);
    }

    public void includeAssociations() {
        this.includeAssociations = true;
    }

    public void excludeAssociations() {
        this.includeAssociations = false;
    }

}
