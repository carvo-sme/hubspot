package ch.carvo.hubspotcrawler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

public class QueryParamBuilder {

    private Map<String, List<String>> params = new LinkedHashMap<>();

    private final String encoding;

    public QueryParamBuilder() {
        this("UTF-8");
    }

    public QueryParamBuilder(String encoding) {
        this.encoding = encoding;
    }

    public QueryParamBuilder add(String name, String value) {
        params.computeIfAbsent(name, k -> new ArrayList<>()).add(value);
        return this;
    }

    public QueryParamBuilder add(QueryParamBuilder otherBuilder) {
        if(otherBuilder != null) {
            params.putAll(otherBuilder.params);
        }
        return this;
    }
    
    public QueryParamBuilder addContactLimits(int limit, int offset) {
        params.computeIfAbsent("count", k -> new ArrayList<>()).add(String.valueOf(offset));
        params.computeIfAbsent("vidOffset", k -> new ArrayList<>()).add(String.valueOf(limit));
        return this;
    }

    public String build() {
        StringBuilder builder = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, List<String>> e : params.entrySet()) {
            List<String> values = e.getValue();
            if(values.isEmpty()) {
                continue;
            }
            String name = e.getKey();
            for(String v : values) {
                if(first) {
                    builder.append('?');
                    first = false;
                } else {
                    builder.append('&');
                }
                builder.append(encode(name)).append("=").append(encode(v));
            }
        }
        return builder.toString();
    }

    private String encode(String str) {
        try {
            return URLEncoder.encode(str, encoding);
        } catch(UnsupportedEncodingException ex) {
            throw new RuntimeException("Unsupported encoding: " + encoding, ex);
        }
    }
}
