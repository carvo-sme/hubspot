package ch.carvo.hubspotcrawler.dto;

@SuppressWarnings("unused")
public class ContactPropertyDTO {

    private String value;

    public ContactPropertyDTO() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
