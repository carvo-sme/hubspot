package ch.carvo.hubspotcrawler.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public enum Property {

    DEAL_NAME("dealname"),
    DEFAULT_LAST_MODIFICATION("lastmodifieddate"),
    FIRST_NAME("vorname"),
    LAST_NAME("nachname"),
    DEAL_STAGE("dealstage"),
    CLOSE_DATE("closedate"),
    CREATE_DATE("createdate"),
    HS_OBJECT_ID("hs_object_id"),
    CONTRACT_CLOSED("contract_closed"),
    CONTRACT_SIGNED("contract_signed"),
    HS_ANALYTICS_SOURCE("hs_analytics_source"),
    CONTACT_FIRST_NAME("firstname"),
    CONTACT_LAST_NAME("lastname"),
    CONTACT_COMPANY("company"),
    CONTACT_CONTACT_LOST("contact_lost"),
    CONTACT_LIFECYCLE_STAGE("lifecyclestage"),
    CONTACT_LEAD_STATUS("hs_lead_status"),
    CONTACT_LEAD_DATE("hs_lifecyclestage_lead_date"),
    CONTACT_CUSTOMER_DATE("hs_lifecyclestage_customer_date"),
    CONTACT_MQL_DATE("hs_lifecyclestage_marketingqualifiedlead_date"),
    CONTACT_SQL_DATE("hs_lifecyclestage_salesqualifiedlead_date"),
    CONTACT_OPP_DATE("hs_lifecyclestage_opportunity_date"),
    CONTACT_SOURCE("quelle"),
    CONTACT_CONTACT_LOST_DATE("contact_lost_date");

    public static final Set<Property> ALL_PROPERTIES;
    private static final Map<String, Property> PROPERTY_INDEX;

    static {
        Set<Property> props = new LinkedHashSet<>();
        props.add(Property.DEFAULT_LAST_MODIFICATION);
        props.add(Property.CONTACT_LEAD_STATUS);
        props.add(Property.CONTACT_CONTACT_LOST);
        props.add(Property.CONTACT_COMPANY);
        props.add(Property.CONTACT_CONTACT_LOST_DATE);
        props.add(Property.CONTACT_FIRST_NAME);
        props.add(Property.CONTACT_LAST_NAME);
        props.add(Property.CONTACT_LEAD_DATE);
        props.add(Property.CONTACT_LIFECYCLE_STAGE);
        props.add(Property.CONTACT_MQL_DATE);
        props.add(Property.CONTACT_SQL_DATE);
        props.add(Property.CONTACT_OPP_DATE);
        props.add(Property.CONTACT_CUSTOMER_DATE);
        props.add(Property.CONTACT_SOURCE);
        props.add(Property.DEAL_NAME);
        props.add(Property.CLOSE_DATE);
        props.add(Property.CREATE_DATE);
        props.add(Property.DEAL_STAGE);
        props.add(Property.HS_ANALYTICS_SOURCE);
        props.add(Property.HS_OBJECT_ID);
        props.add(Property.FIRST_NAME);
        props.add(Property.LAST_NAME);
        props.add(Property.CONTRACT_CLOSED);
        props.add(Property.CONTRACT_SIGNED);
        ALL_PROPERTIES = Collections.unmodifiableSet(props);
        PROPERTY_INDEX = ALL_PROPERTIES.stream().collect(Collectors.toMap(Property::toString, p -> p));
    }

    private String name;

    Property(String name) {
        this.name = name;
    }

    @JsonValue
    public String toString() {
        return this.name;
    }

    @JsonCreator
    public static Property fromString(String name) {
        return PROPERTY_INDEX.get(name);
    }
}
