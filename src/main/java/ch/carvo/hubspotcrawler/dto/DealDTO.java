package ch.carvo.hubspotcrawler.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class DealDTO extends BaseDTO {

    private Long dealId;
    private Long portalId;
    @JsonProperty("isDeleted")
    private Boolean deleted;
    private Map<String, List<Long>> associations;

    private Map<String, Object> unknownProperties = new LinkedHashMap<>();

    public DealDTO() {
    }

    public Long getDealId() {
        return dealId;
    }

    public void setDealId(Long dealId) {
        this.dealId = dealId;
    }

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Map<String, List<Long>> getAssociations() {
        return associations;
    }

    public void setAssociations(Map<String, List<Long>> associations) {
        this.associations = associations;
    }

    @JsonAnySetter
    public void setUnknownProperty(String key, Object object) {
        unknownProperties.put(key, object);
    }

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    public Long getAssociatedVid() {
        if (associations.get("associatedVids") == null) {
            return 0L;
        }
        return associations.get("associatedVids").get(0);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("DealDTO {");
        builder.append("dealId = ").append(dealId);
        builder.append(", portalId = ").append(portalId);
        return builder.append("}").toString();
    }
}
