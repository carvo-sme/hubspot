package ch.carvo.hubspotcrawler.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactDTO extends BaseDTO {

    private Date addedAt;
    private Long vid;
    @JsonProperty("portal-id")
    private Long portalId;
    @JsonProperty("lastmodifieddate")
    private String lastmodifieddate;

    public ContactDTO() {
    }

    public Date getAddedAt() {
        return addedAt;
    }

    public void setAddedAt(Date addedAt) {
        this.addedAt = addedAt;
    }

    public Long getVid() {
        return vid;
    }

    public void setVid(Long vid) {
        this.vid = vid;
    }

    public Long getPortalId() {
        return portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public String getLastmodifieddate() {
        return lastmodifieddate;
    }

    public void setLastmodifieddate(String lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }
}
