package ch.carvo.hubspotcrawler.dto;

import java.sql.Timestamp;
import java.util.Map;

public class BaseDTO {

    private Map<Property, PropertyDTO> properties;

    public Map<Property, PropertyDTO> getProperties() {
        return properties;
    }

    public void setProperties(Map<Property, PropertyDTO> properties) {
        this.properties = properties;
    }

    public String getProperty(Property p) {
        if (properties.get(p) == null) {
            return "";
        }
        return properties.get(p).getValue();
    }

    public Long getPropertyNameAsLong(Property p) {
        if (properties.get(p) == null) {
            return null;
        }
        try {
            return Long.parseLong(properties.get(p).getValue());
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public Timestamp getPropertyAsTimestamp(Property p) {
        Long ts = getPropertyNameAsLong(p);

        if (ts == null || ts.equals(0L)) {
            return null;
        }
        return new Timestamp(getPropertyNameAsLong(p));
    }
}
