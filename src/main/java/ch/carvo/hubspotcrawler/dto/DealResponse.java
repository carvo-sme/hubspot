package ch.carvo.hubspotcrawler.dto;

import java.util.ArrayList;
import java.util.List;

@HapiPath("/deals/v1/deal/paged")
public class DealResponse {

    private List<DealDTO> deals = new ArrayList<>();
    private Boolean hasMore;
    private Long offset;

    public DealResponse() {
    }

    public List<DealDTO> getDeals() {
        return deals;
    }

    public void setDeals(List<DealDTO> deals) {
        this.deals = deals;
    }

    public Boolean getHasMore() {
        return false;
    }

    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }
}
