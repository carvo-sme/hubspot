package ch.carvo.hubspotcrawler.dto;

import ch.carvo.hubspotcrawler.dto.OwnerDTO;
import java.util.ArrayList;
import java.util.List;

@HapiPath(value = "/owners/v2/owners")
public class OwnerResponse {

    private List<OwnerDTO> owners = new ArrayList<>();

    public OwnerResponse() {
    }

    public List<OwnerDTO> getOwners() {
        return owners;
    }

    public void setOwners(List<OwnerDTO> owners) {
        this.owners = owners;
    }
}
