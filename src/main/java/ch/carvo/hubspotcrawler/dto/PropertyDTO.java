package ch.carvo.hubspotcrawler.dto;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PropertyDTO {

    private String name;
    private String value;
    private Date timestamp;
    private String source;
    private String sourceId;
    private List<PropertyDTO> versions;

    private Map<String, Object> unknownProperties = new LinkedHashMap<>();

    public PropertyDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public List<PropertyDTO> getVersions() {
        return versions;
    }

    public void setVersions(List<PropertyDTO> versions) {
        this.versions = versions;
    }

    @JsonAnySetter
    public void setUnknownProperty(String key, Object object) {
        unknownProperties.put(key, object);
    }

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }
}
