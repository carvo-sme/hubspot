package ch.carvo.hubspotcrawler.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@HapiPath("/contacts/v1/lists/all/contacts/all")
public class ContactResponse {
    private List<ContactDTO> contacts;
    @JsonProperty("has-more")
    private Boolean hasMore;
    @JsonProperty("vid-offset")
    private Long vidOffset;

    public ContactResponse() {
    }

    public List<ContactDTO> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactDTO> contacts) {
        this.contacts = contacts;
    }

    public Boolean getHasMore() {
        return hasMore;
    }

    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    public Long getVidOffset() {
        return vidOffset;
    }

    public void setVidOffset(Long vidOffset) {
        this.vidOffset = vidOffset;
    }
}
